### Description

Hijri Date Time and Prayer is an Islamic extension for Gnome Shell.

### Features

- List 5 prayer times
- Displays: Sunrise, Sunset, Midnight
- Show remaining time for the upcoming prayer
- Show current date in Hijri calendar
- Display a notification when it's time for prayer
- Play call for prayer sound file
- Adjustment of Hijri date
- Automatic location detection

### Installation

1. Download zip file: https://extensions.gnome.org/extension/26/islamic-datetime-functions/

2. Extract to ~/.local/share/gnome-shell/extensions/

### License

Licensed under the GNU General Public License, version 3

### Third-Party Assets & Components

- [Libitl](https://github.com/arabeyes-org/ITL)

